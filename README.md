# Archlinux tweaks

## Pacman
### Parallel package download
Edit file `/etc/pacman.conf` and add line `ParallelDownloads = 20` under `[options]` section.  
You can set different number, ofcourse.
